#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NFIELDS 9
#define STRSIZE 100

int main(void)
{
    char input_file[] = "stateoutflow0708.txt";
    char state_code_origin[STRSIZE];
    char county_code_origin[STRSIZE];
    char state_code_destination[STRSIZE];
    char county_code_destination[STRSIZE];
    char state_abbreviation[STRSIZE];
    char state_name[STRSIZE];
    char line[STRSIZE*NFIELDS];
    int return_number = 0;
    int exempt_number = 0;
    int aggr_agi = 0;
    unsigned  total = 0;

    FILE* fp = fopen(input_file, "r");
    if(fp == NULL)
    {
        fprintf(stderr, "File open error");
        return 1;
    }

    fgets(line, NFIELDS*STRSIZE, fp);
    printf("%-30s %7s\n", "STATE", " TOTAL");
    printf("----------------------------------------------------\n");

    while(fgets(line, NFIELDS*STRSIZE, fp) != NULL)
    {
        sscanf(line, "%s %s %s %s %s %s %d %d %d", state_code_origin, county_code_origin,
                state_code_destination, county_code_destination, state_abbreviation,
                state_name, &return_number, &exempt_number, &aggr_agi);
        if(strcmp(state_code_origin, "\"25\"") == 0)
        {
            printf("%-30s %7u\n", state_name, aggr_agi);
            total += aggr_agi;
        }
    }
    printf("----------------------------------------------------\n");
    printf("%-30s %7u\n", "Total", total);
    fclose(fp);
}

